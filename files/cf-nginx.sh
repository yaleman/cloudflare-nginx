#!/bin/bash

# ACL management script for cloudflare-only hosts
# pulls the IP lists and makes a file able to be imported into an nginx config file to deny everyone but cloudflare IPs
# include /etc/nginx/cloudflare-acl.conf into a location $path {} statement

TMPDIR=$(mktemp -d)

CONFIGFILE="$TMPDIR/cloudflare-acl.conf"

touch $CONFIGFILE
curl -s https://www.cloudflare.com/ips-v4 | sed -e "s/^\(.*\)/allow \1;/" >> $CONFIGFILE
curl -s https://www.cloudflare.com/ips-v6 | sed -e "s/^\(.*\)/allow \1;/" >> $CONFIGFILE
echo "deny all;" >> $CONFIGFILE

sudo mv $CONFIGFILE /etc/nginx/

rm -rf "$TMPDIR"

