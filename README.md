# cloudflare-nginx

A simple ansible role for taking the CloudFlare IP lists and making an nginx ACL to load.

`include /etc/nginx/cloudflare-acl.conf` in an nginx config and you'll automagically 403 connections from IPs other than cloudflare.
